#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVector>
#include <QString>
#include <QMultiMap>
#include <QDate>
#include <QMessageBox>
#include "person.h"
#include <QDebug>
#include <QFileDialog>

#include <contact_picker_dialog.h>
#include <all_data_dialog.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:

    void on_listWidget_customContextMenuRequested(const QPoint &pos);

    void on_treeWidget_customContextMenuRequested(const QPoint &pos);

    void deleteListItem();

    void deleteTreeItem();

    void addContact();

    void on_pushButton_add_clicked();

    void on_listWidget_itemSelectionChanged();

    void on_comboBox_activated(const QString &arg1);

    void on_pushButton_viewData_clicked();

    void on_dateEdit_userDateChanged(const QDate &date);

    void on_dateEdit_2_userDateChanged(const QDate &date);

    void on_actionImport_names_triggered();

private:
    Ui::MainWindow *ui;
    contact_picker_dialog* contact_window;
    all_data_dialog* data_window;

    QVector<Person> data {};

    void refreshTree();
    void refresh_contacts2();
};

#endif // MAINWINDOW_H
