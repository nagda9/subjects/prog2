#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->listWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->listWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
    ui->treeWidget->setSelectionMode(QAbstractItemView::ExtendedSelection);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_listWidget_customContextMenuRequested(const QPoint &pos)
{
    // Handle global position
    QPoint globalPos = ui->listWidget->mapToGlobal(pos);

    // Create menu and insert some actions
    QMenu myMenu;
    myMenu.addAction("Set contact", this, SLOT(addContact()));
    myMenu.addAction("Delete",  this, SLOT(deleteListItem()));

    // Show context menu at handling position
    myMenu.exec(globalPos);
}

void MainWindow::deleteListItem()
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    if (!items.isEmpty())
    {
        foreach(QListWidgetItem * item, items)
        {
            for (int i = 0; i < data.size(); ++i){
                if (data[i].name == item->text())
                {
                    data.erase(data.begin()+i);
                }
            }
            delete ui->listWidget->takeItem(ui->listWidget->row(item));
        }
    }
}

void MainWindow::addContact(){
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    QVector<int> people_indexes;

    if (!items.isEmpty())
    {
        foreach(QListWidgetItem * item, items)
        {
            people_indexes.push_back(ui->listWidget->row(item));
        }
    }

    contact_window = new contact_picker_dialog(this, &data, people_indexes);
    contact_window->setModal(true);
    if (contact_window->exec() == QDialog::Accepted) {
        refreshTree();
    }
}

void MainWindow::refresh_contacts2(){
    for (int i = 0; i < data.size(); ++i){
        data[i].contacts2.clear();
        for (auto c : data[i].contacts.uniqueKeys()){
            for (auto v : data[i].contacts.values(c))
            {
                data[i].contacts2.insert(v, c);
            }
        }
    }
}

void MainWindow::refreshTree() {
    ui->treeWidget->clear();

    if (ui->comboBox->currentText() == "A-Z"){
        refresh_contacts2();
    }

    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    if (!items.isEmpty())
    {
        foreach(QListWidgetItem * item, items)
        {
            int index = ui->listWidget->row(item);
            QTreeWidgetItem *rootitem = new QTreeWidgetItem(ui->treeWidget);
            rootitem->setText(0, data[index].name);
            ui->treeWidget->addTopLevelItem(rootitem);

            if (ui->comboBox->currentText() == "Date"){
                for (auto c : data[index].contacts.uniqueKeys())
                {
                    if (ui->dateEdit->date() < c && c < ui->dateEdit_2->date()){
                        for (auto v : data[index].contacts.values(c))
                        {
                            QTreeWidgetItem *child = new QTreeWidgetItem();
                            child->setText(0, v);
                            child->setText(1, c.toString("yyyy.MM.dd"));
                            rootitem->addChild(child);
                        }
                    }
                }
            } else if (ui->comboBox->currentText() == "A-Z"){
                for (auto c : data[index].contacts2.uniqueKeys())
                {
                    for (auto v : data[index].contacts2.values(c))
                    {
                        if (ui->dateEdit->date() < v && v < ui->dateEdit_2->date()) {
                            QTreeWidgetItem *child = new QTreeWidgetItem();
                            child->setText(0, c);
                            child->setText(1, v.toString("yyyy.MM.dd"));
                            rootitem->addChild(child);
                        }
                    }
                }
            }
        }
    }
    ui->treeWidget->expandAll();
}

void MainWindow::on_treeWidget_customContextMenuRequested(const QPoint &pos)
{
    // Handle global position
    QPoint globalPos = ui->treeWidget->mapToGlobal(pos);

    // Create menu and insert some actions
    QMenu myMenu;
    myMenu.addAction("Delete",  this, SLOT(deleteTreeItem()));

    // Show context menu at handling position
    myMenu.exec(globalPos);
}

void MainWindow::deleteTreeItem()
{
    QList<QTreeWidgetItem *> items = ui->treeWidget->selectedItems();
    QTreeWidgetItem *pp = nullptr;

    if (!items.isEmpty())
    {
        foreach (QTreeWidgetItem *item, items)
        {
            if (item->parent() != nullptr){
                pp = item->parent();
                pp->removeChild(item);

                QDate date = QDate::fromString(item->text(1), "yyyy.MM.dd");
                for (int i = 0; i < data.size(); ++i)
                {
                    if (data[i].name == pp->text(0)) {
                        data[i].contacts.remove(date, item->text(0));
                    }
                }
                delete item;
            }
            else
            {
                for (int i = item->childCount(); i >= 0; --i){
                    for (int i = 0; i < data.size(); ++i)
                    {
                        if (data[i].name == item->text(0)) {
                            data[i].contacts.clear();
                        }
                    }
                    item->removeChild(item->child(i));
                }
            }
        }
    }
}

void MainWindow::on_pushButton_add_clicked()
{
    QString name = ui->lineEdit_name->text();
    name.replace("  ", "");
    bool isUsed = false;
    for (int i = 0; i < data.size(); ++i)
    {
        if (data[i].name == name)
        {
            isUsed = true;
        }
    }
    if (name != "" && name != " " && name[0] != " " && isUsed == false)
    {
        ui->listWidget->addItem(name);
        ui->lineEdit_name->clear();
        Person p(name);
        data.push_back(p);
    }
    else {
        QMessageBox::warning(this, "Warning", "Enter a valid name!");
        ui->lineEdit_name->clear();
    }
}

void MainWindow::on_listWidget_itemSelectionChanged()
{
    refreshTree();
}

void MainWindow::on_comboBox_activated(const QString &arg1)
{
    refreshTree();
}

void MainWindow::on_pushButton_viewData_clicked()
{
    refresh_contacts2();
    data_window = new all_data_dialog(this, data);
    data_window->setModal(true);
    data_window->exec();
}

void MainWindow::on_dateEdit_userDateChanged(const QDate &date)
{
    refreshTree();
}

void MainWindow::on_dateEdit_2_userDateChanged(const QDate &date)
{
    refreshTree();
}

void MainWindow::on_actionImport_names_triggered()
{
    ui->listWidget->clear();
    data.clear();

    QString filePath = QFileDialog::getOpenFileName(this, "Open the file");
    QFile file (filePath);

    if (!file.open(QFile::ReadOnly))
    {
        QMessageBox::critical(this, "Warning", "No files are found!");
    }
    QTextStream stream(&file);
    stream.setCodec("UTF-8");
    QString line;
    while (stream.readLineInto(&line))
    {
        ui->listWidget->addItem(line);
        Person p(line);
        data.push_back(p);
    }
    file.close();
}
