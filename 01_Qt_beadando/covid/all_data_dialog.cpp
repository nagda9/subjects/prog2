#include "all_data_dialog.h"
#include "ui_all_data_dialog.h"

all_data_dialog::all_data_dialog(QWidget *parent, const QVector<Person>& data) :
    QDialog(parent),
    ui(new Ui::all_data_dialog)
{
    ui->setupUi(this);

    for (int i = 0; i < data.size(); ++i) {
        ui->plainTextEdit->appendPlainText(data[i].name);
        int n = 0;
        for (auto c : data[i].contacts2.uniqueKeys())
        {
            for(auto v : data[i].contacts2.values(c)){
                ui->plainTextEdit->appendPlainText("  " + QString::number(n) + "  Kivel: " + c + "  Mikor: " + v.toString("yyyy.MM.dd"));
                n++;
            }
        }
    }
}

all_data_dialog::~all_data_dialog()
{
    delete ui;
}

void all_data_dialog::on_pushButton_clicked()
{
    QDialog::close();
}
