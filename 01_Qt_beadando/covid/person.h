#ifndef PERSON_H
#define PERSON_H

#include <QString>
#include <QDate>
#include <QMultiMap>
#include <QVector>

class Person{
public:
    QString name {};
    QMultiMap<QDate, QString> contacts {};
    QMultiMap<QString, QDate> contacts2 {};
    Person() = default;
    Person(QString n) : name(n) {};
};

#endif // PERSON_H
