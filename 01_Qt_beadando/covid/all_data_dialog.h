#ifndef ALL_DATA_DIALOG_H
#define ALL_DATA_DIALOG_H

#include <QDialog>
#include <QVector>
#include <person.h>
#include <QDebug>

namespace Ui {
class all_data_dialog;
}

class all_data_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit all_data_dialog(QWidget *parent, const QVector<Person>& data);
    ~all_data_dialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::all_data_dialog *ui;
};

#endif // ALL_DATA_DIALOG_H
