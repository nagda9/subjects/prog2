#include "contact_picker_dialog.h"
#include "ui_contact_picker_dialog.h"

contact_picker_dialog::contact_picker_dialog(QWidget *parent, QVector<Person>* pdata_, const QVector<int>& people_indexes_) :
    QDialog(parent),
    ui(new Ui::contact_picker_dialog),
    people_indexes(people_indexes_)
{
    ui->setupUi(this);
    ui->stackedWidget->setCurrentIndex(0);
    ui->listWidget_contact->setSelectionMode(QAbstractItemView::ExtendedSelection);

    pdata = pdata_;
    fill_listWidget();
}

contact_picker_dialog::~contact_picker_dialog()
{
    delete ui;
}

void contact_picker_dialog::fill_listWidget(){
    for (int i = 0; i < pdata->size(); ++i)
    {
        bool b = false;
        for (int j = 0; j < people_indexes.size(); ++j)
        {
            if ((*pdata)[i].name == (*pdata)[people_indexes[j]].name)
            {
                b = true;
            }
        }
        if (b == false)
        {
            ui->listWidget_contact->addItem((*pdata)[i].name);
        }
    }
}

void contact_picker_dialog::on_pushButton_clicked()
{
    QDialog::accept();
}

void contact_picker_dialog::on_pushButton_2_clicked()
{
    contact_items = ui->listWidget_contact->selectedItems();
    if (contact_items.size() > 0) {
        ui->stackedWidget->setCurrentIndex(1);
    }
}

void contact_picker_dialog::on_pushButton_4_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

void contact_picker_dialog::on_pushButton_3_clicked()
{
    if (!ui->calendarWidget->selectedDate().isNull())
    {
        QDate time = ui->calendarWidget->selectedDate();

        for (int i = 0; i < people_indexes.size(); ++i)
        {
            for (int j = 0; j < contact_items.size(); ++j)
            {
                (*pdata)[people_indexes[i]].contacts.insert(time, contact_items[j]->text());
            }
        }
        ui->stackedWidget->setCurrentIndex(0);
        QMessageBox::warning(this, "success", "Saved successfully!");
    }
    else
    {
        QMessageBox::warning(this, "Warning", "Choose date!");
    }
}
