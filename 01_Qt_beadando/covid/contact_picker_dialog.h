#ifndef CONTACT_PICKER_DIALOG_H
#define CONTACT_PICKER_DIALOG_H

#include <QDialog>
#include <person.h>
#include <QVector>
#include <QList>
#include <QListWidgetItem>
#include <QMessageBox>
#include <iostream>
#include <QDebug>

namespace Ui {
class contact_picker_dialog;
}

class contact_picker_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit contact_picker_dialog(QWidget*parent, QVector<Person>* pdata_, const QVector<int>& people_indexes_);
    ~contact_picker_dialog();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::contact_picker_dialog *ui;

    const QVector<int> people_indexes;
    QList<QListWidgetItem*> contact_items;
    QVector<Person>* pdata;

    void fill_listWidget();
};

#endif // CONTACT_PICKER_DIALOG_H
