#ifndef READY_DIALOG_H
#define READY_DIALOG_H

#include <QDialog>
#include <QTimer>

namespace Ui {
class Ready_dialog;
}

class Ready_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Ready_dialog(QWidget *parent = 0);
    ~Ready_dialog();

private slots:
    void ready_counter();

private:
    Ui::Ready_dialog *ui;
    QTimer *countBack;

    int counter = 3;
};

#endif // READY_DIALOG_H
