#ifndef UJ_KERDES_DIALOG_H
#define UJ_KERDES_DIALOG_H

#include <QDialog>

namespace Ui {
class uj_kerdes_dialog;
}

class uj_kerdes_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit uj_kerdes_dialog(QWidget *parent = 0);
    ~uj_kerdes_dialog();

    QString get_title();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    //void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::uj_kerdes_dialog *ui;
    QString title;
};

#endif // UJ_KERDES_DIALOG_H
