#ifndef EREDMENY_DIALOG_H
#define EREDMENY_DIALOG_H

#include <QDialog>

namespace Ui {
class Eredmeny_dialog;
}

class Eredmeny_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Eredmeny_dialog(QWidget *parent, const int& pontszam, const int& size, const QString& nev);
    ~Eredmeny_dialog();

private slots:
    void on_pushButton_clicked();
    
private:
    Ui::Eredmeny_dialog *ui;
};

#endif // EREDMENY_DIALOG_H
