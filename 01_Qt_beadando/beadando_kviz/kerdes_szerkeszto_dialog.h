#ifndef KERDES_SZERKESZTO_DIALOG_H
#define KERDES_SZERKESZTO_DIALOG_H

#include <QDialog>
#include "kerdesek_class.h"

namespace Ui {
class kerdes_szerkeszto_dialog;
}

class kerdes_szerkeszto_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit kerdes_szerkeszto_dialog(QWidget *parent = 0);
    kerdes_szerkeszto_dialog(QWidget *parent, Kerdesek_class& regi_kerdes);
    ~kerdes_szerkeszto_dialog();

    Kerdesek_class get_kerdes();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::kerdes_szerkeszto_dialog *ui;
    Kerdesek_class uj_kerdes;
};

#endif // KERDES_SZERKESZTO_DIALOG_H
