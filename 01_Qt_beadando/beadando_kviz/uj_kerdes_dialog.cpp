#include "uj_kerdes_dialog.h"
#include "ui_uj_kerdes_dialog.h"
#include "mainwindow.h"

uj_kerdes_dialog::uj_kerdes_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::uj_kerdes_dialog)
{
    ui->setupUi(this);
}

uj_kerdes_dialog::~uj_kerdes_dialog()
{
    delete ui;
}

QString uj_kerdes_dialog::get_title()
{
    return title;
}

void uj_kerdes_dialog::on_buttonBox_accepted()
{
    title = ui->lineEdit->text();
}

void uj_kerdes_dialog::on_buttonBox_rejected()
{
    return;
}
