#include "ready_dialog.h"
#include "ui_ready_dialog.h"

Ready_dialog::Ready_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Ready_dialog)
{
    ui->setupUi(this);
    countBack = new QTimer(this);
    connect(countBack, SIGNAL(timeout()), this, SLOT(ready_counter()));
    countBack->start(1000);
}

Ready_dialog::~Ready_dialog()
{
    delete ui;
}

void Ready_dialog::ready_counter()
{
    --counter;
    if (counter == 0)
    {
        this->accept();
        QDialog::close();
    }
    ui->label_num->setText(QString::number(counter));
}
