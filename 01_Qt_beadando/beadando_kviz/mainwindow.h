#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "uj_kerdes_dialog.h"
#include "about_dialog.h"
#include "kerdes_szerkeszto_dialog.h"
#include "kerdesek_class.h"
#include "eredmeny_dialog.h"
#include "ready_dialog.h"

#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QTimer>
#include <QListWidgetItem>
#include <QButtonGroup>


namespace Ui {
class MainWindow;

}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionExit_triggered();

    void on_actionK_rd_ssor_kiv_laszt_sa_triggered();

    void on_jatek_clicked();

    void on_action_j_j_t_k_ind_t_sa_triggered();

    void on_actionKil_p_s_a_j_t_kb_l_triggered();

    void on_actionSzerkeszt_s_triggered();

    void on_btn_fooldal_clicked();

    void on_action_j_kv_z_l_trehoz_sa_triggered();

    void on_actionAbout_triggered();

    void on_stackedWidget_currentChanged(int arg1);

    void on_comboBox_kerdessor_valaszto_activated(const QString &arg1);

    void on_btn_new_clicked();

    void on_btn_edit_clicked();

    void on_btn_del_clicked();

    void on_btn_save_clicked();

    void on_pushButton_next_clicked();

    void timer_change();

    void on_listWidget_files_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::MainWindow *ui;
    uj_kerdes_dialog *uj_kerdes_window;
    about_dialog *about_window;
    kerdes_szerkeszto_dialog *kerdes_szerk_window;
    Eredmeny_dialog *eredmeny_window;
    Ready_dialog *ready_window;
    QTimer *timer;

    QString projectDir = "../beadando_kviz/Kerdessorok";
    QString currentFile = "próba_kérdéssor.qvz";
    QString aktualis_kerdessor_neve = "";
    QVector<Kerdesek_class> aktualis_kerdessor;
    int aktualis_kerdessor_index = 0;
    int remaining_time = 60;

    QString jatekos_nev = "";
    int pontszam = 0;

    void beolvas(QFile& file);
    void kiir(QFile& file);
    void check_open_readonly(QFile& file);
    void check_open_writeonly(QFile& file);
    void refresh_file(QFile& file);
    void setcombobox();
    QString convert(QString text);
};

#endif // MAINWINDOW_H
