#include "eredmeny_dialog.h"
#include "ui_eredmeny_dialog.h"

Eredmeny_dialog::Eredmeny_dialog(QWidget *parent, const int& pontszam, const int& size, const QString& nev) :
    QDialog(parent),
    ui(new Ui::Eredmeny_dialog)
{
    ui->setupUi(this);
    ui->label_eredmeny->setText("A helyes válaszaid: " + QString::number(pontszam) + "/" + QString::number(size));
    ui->label_nev->setText("Gratulálok " + nev + "!");
}

Eredmeny_dialog::~Eredmeny_dialog()
{
    delete ui;
}

void Eredmeny_dialog::on_pushButton_clicked()
{
    this->accept();
    QDialog::close();
}
