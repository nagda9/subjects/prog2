#include "kerdes_szerkeszto_dialog.h"
#include "ui_kerdes_szerkeszto_dialog.h"

kerdes_szerkeszto_dialog::kerdes_szerkeszto_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::kerdes_szerkeszto_dialog)
{
    ui->setupUi(this);
}

kerdes_szerkeszto_dialog::kerdes_szerkeszto_dialog(QWidget *parent, Kerdesek_class& regi_kerdes):
    QDialog(parent),
    ui(new Ui::kerdes_szerkeszto_dialog),
    uj_kerdes(regi_kerdes)
{
    ui->setupUi(this);
    ui->lineEdit_Q->setText(regi_kerdes.ques);
    ui->lineEdit_A->setText(regi_kerdes.answ['A']);
    ui->lineEdit_B->setText(regi_kerdes.answ['B']);
    ui->lineEdit_C->setText(regi_kerdes.answ['C']);
    ui->lineEdit_D->setText(regi_kerdes.answ['D']);
    ui->comboBox_answers->setCurrentText(regi_kerdes.correct);
}

Kerdesek_class kerdes_szerkeszto_dialog::get_kerdes()
{
    return uj_kerdes;
}

kerdes_szerkeszto_dialog::~kerdes_szerkeszto_dialog()
{
    delete ui;
}

void kerdes_szerkeszto_dialog::on_buttonBox_accepted()
{
    uj_kerdes.ques = ui->lineEdit_Q->text();
    uj_kerdes.answ['A'] = ui->lineEdit_A->text();
    uj_kerdes.answ['B'] = ui->lineEdit_B->text();
    uj_kerdes.answ['C'] = ui->lineEdit_C->text();
    uj_kerdes.answ['D'] = ui->lineEdit_D->text();
    uj_kerdes.correct = ui->comboBox_answers->currentText()[0];
    this->accept();
}

void kerdes_szerkeszto_dialog::on_buttonBox_rejected()
{
    this->reject();
}
