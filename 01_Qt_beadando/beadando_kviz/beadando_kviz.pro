#-------------------------------------------------
#
# Project created by QtCreator 2019-10-30T22:27:03
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = beadando_kviz
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    uj_kerdes_dialog.cpp \
    about_dialog.cpp \
    kerdesek_class.cpp \
    kerdes_szerkeszto_dialog.cpp \
    eredmeny_dialog.cpp \
    ready_dialog.cpp

HEADERS += \
        mainwindow.h \
    uj_kerdes_dialog.h \
    about_dialog.h \
    kerdesek_class.h \
    kerdes_szerkeszto_dialog.h \
    eredmeny_dialog.h \
    ready_dialog.h

FORMS += \
        mainwindow.ui \
    uj_kerdes_dialog.ui \
    about_dialog.ui \
    kerdes_szerkeszto_dialog.ui \
    eredmeny_dialog.ui \
    ready_dialog.ui

RESOURCES += \
    resources.qrc
