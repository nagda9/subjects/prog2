#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <iostream>
#include <sstream>

/***** Initialize *****/
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    if (QDir("../beadando_kviz/Kerdessorok").exists() == false)
    {
        QDir("../beadando_kviz").mkdir("Kerdessorok");
    }
    ui->stackedWidget->setCurrentIndex(0);
    ui->stackedWidget->currentChanged(0);
}

MainWindow::~MainWindow()
{
    delete ui;
}

/***** Setup *****/
void MainWindow::on_stackedWidget_currentChanged(int arg1)
{
    if (arg1==0)
    {
        ui->listWidget_files->clear();
        QDir dir(projectDir);
        QStringList kerdessorok = dir.entryList(QStringList() << "*.qvz", QDir::Files);
        foreach (QString fileName, kerdessorok)
        {
            QFile file(projectDir + "/" + fileName);
            check_open_readonly(file);
            QTextStream in(&file);
            QString line = in.readLine();
            line.remove(0, 5);
            ui->listWidget_files->addItem(line);
        }

        QFile file (dir.absoluteFilePath(currentFile));
        beolvas(file);
        ui->label_4->setText(aktualis_kerdessor_neve);
    }
    if (arg1==1)
    {
        if(aktualis_kerdessor.size() != 0)
        {
            remaining_time = 60;
            if (aktualis_kerdessor_index == 0)
            {
                ready_window = new Ready_dialog(this);
                ready_window->setModal(true);
                if(ready_window->exec() == QDialog::Accepted)
                {
                    timer = new QTimer(this);
                    connect(timer, SIGNAL(timeout()), this, SLOT(timer_change()));
                    timer->start(1000);
                }
            }

            ui->label_title->setText(aktualis_kerdessor_neve);
            ui->radioButton_ans1->setChecked(true);
            ui->label_page->setText(QString::number(aktualis_kerdessor_index + 1) + "/" + QString::number(aktualis_kerdessor.size()));
            ui->groupBox->setTitle(aktualis_kerdessor[aktualis_kerdessor_index].ques);
            ui->radioButton_ans1->setText(aktualis_kerdessor[aktualis_kerdessor_index].answ['A']);
            ui->radioButton_ans2->setText(aktualis_kerdessor[aktualis_kerdessor_index].answ['B']);
            ui->radioButton_ans3->setText(aktualis_kerdessor[aktualis_kerdessor_index].answ['C']);
            ui->radioButton_ans4->setText(aktualis_kerdessor[aktualis_kerdessor_index].answ['D']);
            ui->label_points->setText(QString::number(pontszam));
        }
        else
        {
            QMessageBox::warning(this, "error", "There are no question in this quiz!");
            ui->stackedWidget->setCurrentIndex(0);
        }
    }
    if (arg1==2)
    {
        setcombobox();
    }

    if (arg1==3)
    {

    }
}

/***** Menu bar *****/
//File
void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_actionK_rd_ssor_kiv_laszt_sa_triggered()
{

    QString filePath = QFileDialog::getOpenFileName(this, "Open the file");
    QFile file (filePath);

    beolvas(file);
    currentFile = convert(aktualis_kerdessor_neve);
    file.copy(filePath, projectDir + "/" + currentFile);
    ui->label_4->setText(aktualis_kerdessor_neve);

}

//Játék
void MainWindow::on_action_j_j_t_k_ind_t_sa_triggered()
{
    on_jatek_clicked();
}

void MainWindow::on_actionKil_p_s_a_j_t_kb_l_triggered()
{
    ui->stackedWidget->setCurrentIndex(0);
    aktualis_kerdessor_index = 0;
}

//Szerkesztő
void MainWindow::on_action_j_kv_z_l_trehoz_sa_triggered()
{
    uj_kerdes_window = new uj_kerdes_dialog(this);
    uj_kerdes_window->setModal(true);
    if (uj_kerdes_window->exec() == QDialog::Accepted)
    {
        aktualis_kerdessor.clear();
        QString title = uj_kerdes_window->get_title();
        QString fileName = convert(title);
        currentFile = fileName;
        aktualis_kerdessor_neve = title;

        QDir dir(projectDir);
        QFile file (dir.absoluteFilePath(currentFile));
        kiir(file);
        file.close();

        ui->stackedWidget->setCurrentIndex(2);
    }
}

void MainWindow::on_actionSzerkeszt_s_triggered()
{
    ui->stackedWidget->setCurrentIndex(2);
}

//Info
void MainWindow::on_actionAbout_triggered()
{
    about_window = new about_dialog(this);
    about_window->setModal(true);
    about_window->show();
}

/***** File handling *****/
void MainWindow::beolvas(QFile& file)
{
    check_open_readonly(file);
    aktualis_kerdessor.clear();
    QTextStream in(&file);
    QString line;
    Kerdesek_class kerdes;

    while(!in.atEnd())
    {
        line = in.readLine();
        if (line.startsWith("Name:"))
        {
            line.remove(0, 5);
            aktualis_kerdessor_neve = line;
        }
        if (line.startsWith("Q:"))
        {
            line.remove(0, 2);
            kerdes.ques = line;
        }
        if (line.startsWith("A:") || line.startsWith("B:") || line.startsWith("C:") || line.startsWith("D:"))
        {
            kerdes.answ[line[0]] = line.mid(2, line.length());
        }
        if (line.startsWith("Ans:"))
        {
            line.remove(0, 4);
            kerdes.correct = line[0];
            aktualis_kerdessor.push_back(kerdes);
        }
    }
    file.close();
}

void MainWindow::kiir(QFile& file)
{
    check_open_writeonly(file);
    QTextStream out(&file);

    out << "Name:" << aktualis_kerdessor_neve << endl;
    if (aktualis_kerdessor.size() > 0)
    {
        for (Kerdesek_class kerdes : aktualis_kerdessor)
        {
            out << "Q:" << kerdes.ques << endl << "A:" << kerdes.answ['A'] << endl << "B:" << kerdes.answ['B'] << endl << "C:" << kerdes.answ['C'] << endl << "D:" << kerdes.answ['D'] << endl << "Ans:" << kerdes.correct << ';' << endl;
        }
    }
    file.close();
}

/***** Page 0 *****/
void MainWindow::on_listWidget_files_itemDoubleClicked(QListWidgetItem *item)
{
    currentFile = convert(item->text());
    aktualis_kerdessor_neve = item->text();
    ui->stackedWidget->currentChanged(0);
}

void MainWindow::on_jatek_clicked()
{
    pontszam = 0;
    aktualis_kerdessor_index = 0;

    if(ui->nev_lineEdit->text()!= "")
    {
        jatekos_nev = ui->nev_lineEdit->text();
        aktualis_kerdessor_index = 0;
        ui->stackedWidget->setCurrentIndex(1);
    }
    else
    {
        QMessageBox::warning(this, "error", "Type in a name!");
        return;
    }   
}

/***** Page 1 *****/
void MainWindow::on_pushButton_next_clicked()
{
    QButtonGroup group;
    QList<QRadioButton *> allButtons = ui->groupBox->findChildren<QRadioButton *>();

    for (int i = 0; i < allButtons.size(); i++)
    {
        group.addButton(allButtons[i],i);
    }

    if(group.checkedButton()->text() == aktualis_kerdessor[aktualis_kerdessor_index].answ[aktualis_kerdessor[aktualis_kerdessor_index].correct])
    {
        ++pontszam;
    }

    if (aktualis_kerdessor.size()-1 > aktualis_kerdessor_index)
    {
        ++aktualis_kerdessor_index;
        ui->stackedWidget->currentChanged(1);
    }
    else
    {
        timer->stop();
        ui->label_points->setText(QString::number(pontszam));
        eredmeny_window = new Eredmeny_dialog(this, pontszam, aktualis_kerdessor.size(), jatekos_nev);
        eredmeny_window->setModal(true);
        if (eredmeny_window->exec() == QDialog::Accepted)
        {
            ui->stackedWidget->setCurrentIndex(0);
        }
    }
}

/***** Page 2 *****/
void MainWindow::on_comboBox_kerdessor_valaszto_activated(const QString &arg1)
{
    ui->listWidget->clear();

    QDir directory(projectDir);
    QStringList kerdessorok = directory.entryList(QStringList() << "*.qvz", QDir::Files);

    foreach (QString fileName, kerdessorok)
    {
        QFile file(projectDir + "/" + fileName);
        check_open_readonly(file);
        QTextStream in(&file);
        QString line = in.readLine();
        line.remove(0, 5);
        if (line == arg1)
        {
            currentFile = fileName;
            QFile file(directory.absoluteFilePath(currentFile));
            beolvas(file);
        }
    }

    for (Kerdesek_class kerdes : aktualis_kerdessor)
    {
        ui->listWidget->addItem(kerdes.ques);
    }
}

void MainWindow::on_btn_edit_clicked()
{
    if (ui->listWidget->count() > 0 && ui->listWidget->selectedItems().size() > 0)
    {
        QString Q = ui->listWidget->currentItem()->text();
        int j;
        for (int i = 0; i < aktualis_kerdessor.size(); i++)
        {
            if(aktualis_kerdessor[i].ques == Q)
            {
                j = i;
            }
        }

        kerdes_szerk_window = new kerdes_szerkeszto_dialog(this, aktualis_kerdessor[j]);
        kerdes_szerk_window->setModal(true);
        if (kerdes_szerk_window->exec() == QDialog::Accepted)
        {
            aktualis_kerdessor[j] = kerdes_szerk_window->get_kerdes();
            ui->listWidget->currentItem()->setText(kerdes_szerk_window->get_kerdes().ques);
        }
    }
    else if(ui->listWidget->count() == 0)
    {
        QMessageBox::warning(this, "error", "Nothing to edit. Create new question!");
        return;
    }
    else
    {
        QMessageBox::warning(this, "error", "Select an item!");
        return;
    }
}

void MainWindow::on_btn_new_clicked()
{
    kerdes_szerk_window = new kerdes_szerkeszto_dialog(this);
    kerdes_szerk_window->setModal(true);
    if (kerdes_szerk_window->exec() == QDialog::Accepted)
    {
        aktualis_kerdessor.push_back(kerdes_szerk_window->get_kerdes());
        ui->listWidget->addItem(kerdes_szerk_window->get_kerdes().ques);
    }
}

void MainWindow::on_btn_del_clicked()
{
    if (ui->listWidget->selectedItems().size() > 0)
    {
        int index = ui->listWidget->currentRow();
        QListWidgetItem *it = ui->listWidget->takeItem(index);
        delete it;
        aktualis_kerdessor.remove(index);
    }
}

void MainWindow::on_btn_save_clicked()
{
    QDir dir(projectDir);
    QFile file (dir.absoluteFilePath(currentFile));
    refresh_file(file);
    QMessageBox::warning(this, "success", "Saved successfully!");
}

void MainWindow::on_btn_fooldal_clicked()
{
    ui->stackedWidget->setCurrentIndex(0);
}

/***** Other functions *****/
void MainWindow::check_open_readonly(QFile& file)
{
    if (!file.open(QIODevice::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Cannot open file: " + file.errorString());
        return;
    }
}

void MainWindow::check_open_writeonly(QFile& file)
{
    if (!file.open(QIODevice::WriteOnly | QFile::Text))
    {
        QMessageBox::warning(this, "Warning", "Cannot open file: " + file.errorString());
        return;
    }
}

void MainWindow::refresh_file(QFile& file) //szerkesztett file esetén
{
    kiir(file);
    on_comboBox_kerdessor_valaszto_activated(ui->comboBox_kerdessor_valaszto->currentText());
}

void MainWindow::setcombobox() //új file esetén
{
    ui->comboBox_kerdessor_valaszto->clear();
    QDir directory(projectDir);
    QStringList kerdessorok = directory.entryList(QStringList() << "*.qvz", QDir::Files);

    foreach (QString fileName, kerdessorok)
    {
        QFile file(projectDir + "/" + fileName);
        check_open_readonly(file);
        QTextStream in(&file);
        QString line = in.readLine();
        line.remove(0, 5);
        QString kerdessor_neve = line;

        ui->comboBox_kerdessor_valaszto->addItem(kerdessor_neve, QComboBox::InsertAlphabetically);
    }
    ui->comboBox_kerdessor_valaszto->setCurrentText(aktualis_kerdessor_neve);
    ui->comboBox_kerdessor_valaszto->activated(aktualis_kerdessor_neve);
}

QString MainWindow::convert(QString text)
{
    for (int i = 0; i < text.length(); i++)
    {
        text[i]=text[i].toLower();
        if (text[i] == " ")
        {
            text.replace(i, 1, "_");
        }
    }

    text.append(".qvz");
    return text;
}

void MainWindow::timer_change()
{
    remaining_time = remaining_time -1;
    if (remaining_time == 0)
    {
        remaining_time = 60;
        on_pushButton_next_clicked();
    }
    ui->label_timer->setText(QString::number(remaining_time));
}
