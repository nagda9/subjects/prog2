#ifndef NAGYBEADANDO_NODE_H
#define NAGYBEADANDO_NODE_H

#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <set>
#include <list>
#include <cmath>
#include "Reader.h"
#include "Akcio.h"

using namespace std;

struct Allomas
{
    map<Vonat, map<Kocsi, map<Cucc,int>>> vonatok;        // vonatban a kocsiban az állomáson a cucc
    map<Kocsi, map<Cucc, int>> kocsik;                    // kocsiban az állomáson a cucc
    map<Cucc, int> cuccok;                                // állomáson a cucc
};

class Node {
private:
    int pontszam;
    int ido;
    vector<Akcio> akcio_diff;
    Node * parent;
    map<string, Allomas> node;
    map<string, map<string, pair<string, int>>> terkep;


public:
    Node();
    void do_actions();
    void move_train();

    ~Node();
    explicit Node(Reader& reader);
    Node(Node* parent_node, const vector<Akcio>& akciok);

    int get_pontszam();
    void set_pontszam(int pont);
    int get_ido();
    Node * get_parent();
    map<string, Allomas> get_node();

    void torol(const string& honnan, Cucc mit);
    void kiir();
    void akcio_kiir();

    bool is_finish();
};


#endif //NAGYBEADANDO_NODE_H
