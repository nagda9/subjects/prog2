#ifndef NAGYBEADANDO_READER_H
#define NAGYBEADANDO_READER_H

#include <iostream>
#include <sstream>
#include <fstream>
#include "Vonat.h"
#include "Kocsi.h"
#include "Cucc.h"

using namespace std;

class Reader {
private:
    vector<Vonat> vonatok;
    vector<Kocsi> kocsik;
    vector<Cucc> cuccok;

public:
    explicit Reader(ifstream& bf);
    static void check_file(ifstream& bf);
    vector<Vonat> get_vonatok() const;
    vector<Kocsi> get_kocsik();
    vector<Cucc> get_cuccok();
};


#endif //NAGYBEADANDO_READER_H
