#include "Graph.h"

// Pointer szabályok:
// - NEM másoljuk! Mindent amiben pointer van, létrehozzuk fv előtt, és referencia szerint átadjuk
// - Pointer átadásakor paraméterben mindig a memóriaterületet adjuk át
// - Ha linked-listet csinálunk, akkor a paraméter átvételekor Tp * pointer kell legyen

Graph::Graph(Node * parent_node, const Reader& reader)
{
    create_terkep(reader);

    for(auto & rossz_cucc : terkep.szomoru)
    {
        cout<<"Sajnos nem lehet kiszallitani: "<<rossz_cucc.get_nev()<<" "<<rossz_cucc.get_honnan()<<" -> "<<rossz_cucc.get_hova()<<" "<<rossz_cucc.get_mennyiseg()<<endl;
    }

    int sum = 0;
    int n = 0;
    for(auto & all_it : (*parent_node).get_node())
    {
        for(auto & cucc_it : all_it.second.cuccok)
        {
            for(auto & rossz_cucc : terkep.szomoru)
            {
                if(rossz_cucc == cucc_it.first)
                {
                    (*parent_node).torol(all_it.first,cucc_it.first);
                }
            }
            sum += cucc_it.second;
            n++;
        }
    }
    chunky = sum / n;

    graph.push_back(parent_node);
    open.push_back(&parent_node);

    int i = 0;
    bool hasEnd = false;
    while(!hasEnd)
    {
        cout << "Round: "<< i << endl;

        auto it = min_open();
        //(***it).kiir();

        list<Node *> new_children;
        create_children(new_children, &***it); //feltöltjük a new_children-t

        auto it_finish = ellenoriz(new_children);

        if (new_children.end() == it_finish) // nem találtunk jó megoldást
        {
            graph.insert(graph.begin(), new_children.begin(), new_children.end());

            open.erase(it);
            for (auto & new_child : new_children)
            {
                Node ** new_open = new Node *(new_child);
                open.emplace_front(new_open);
            }
        }
        else
        {
            list<Node *> eredmeny;
            hasEnd = true;
            Node * current_node = *it_finish;
            while((*current_node).get_parent() != nullptr)
            {
                eredmeny.push_front(current_node);
                current_node = (*current_node).get_parent();
            }

            cout << endl <<"Round: "<< i << endl;

            for(auto & er_it : eredmeny)
            {
                (*er_it).akcio_kiir();
            }
        }
        i++;
    }
}

void Graph::create_children(list<Node *>& children, Node * parent_node)
{
    vector<vector<Akcio>> akciok = {{}};

    lecsatol(parent_node, akciok);
    lepakol(parent_node, akciok);
    felpakol(parent_node, akciok);
    felcsatol(parent_node, akciok);

    for(auto & vit : akciok)
    {
        Node * child = new Node(parent_node, vit);
        child->set_pontszam(child->get_ido() + heur(&*child));
        children.emplace_back(child);
    }
}

list<Node *>::iterator Graph::ellenoriz(list<Node *>& nodes)
{
    for (auto it = nodes.begin(); it != nodes.end(); it++)
    {
        if ((**it).is_finish())
        {
            return it;
        }
    }
    return nodes.end();
}

list<Node **>::iterator Graph::min_open()
{
    auto it_min = open.begin();

    for (auto it = open.begin(); it != open.end(); it++)
    {
        if((***it_min).get_pontszam() > (***it).get_pontszam())
        {
            it_min = it;
        }
    }
    return it_min;
}

int Graph::heur(Node * node)
{
    int szabadkocs = 0;
    int szabadvon = 0;
    int cuccutazik = 0;
    int kocsiutazik = 0;
    int rossz_vonatra = 0;

    for(auto & all_it : node->get_node())
    {
        for(auto & von_it : all_it.second.vonatok)
        {
            szabadvon += von_it.first.get_kapacitas();
            for(auto & kocs_it : von_it.second)
            {
                szabadvon--;
                szabadkocs += kocs_it.first.get_kapacitas();
                for(auto &cucc_it : kocs_it.second)
                {
                    jo_vonat(node->get_ido(), all_it.first, cucc_it.first.get_hova());

                    kocsiutazik += cucc_it.second;
                    cuccutazik += cucc_it.second;
                    szabadkocs -= cucc_it.second;
                }
            }
        }
        for(auto & kocs_it : all_it.second.kocsik)
        {
            szabadkocs += kocs_it.first.get_kapacitas();
            for(auto & cucc_it : kocs_it.second)
            {
                jo_vonat(node->get_ido(), all_it.first, cucc_it.first.get_hova());

                if(cucc_it.first.get_hova() != all_it.first)
                {
                    kocsiutazik += cucc_it.second;
                }
                cuccutazik += cucc_it.second;
                szabadkocs -= cucc_it.second;
            }
        }
        for(auto & cucc_it : all_it.second.cuccok)
        {
            if(cucc_it.first.get_hova() != all_it.first)
            {
                kocsiutazik += cucc_it.second;
                cuccutazik += cucc_it.second;
            }
        }
    }
    return 3*cuccutazik+2*kocsiutazik+szabadkocs+szabadvon; //  + node->get_ido()
}

void Graph::create_terkep(Reader reader)
{
    set<string> kaki;
    vector<int> t_max;
    for(auto & von_it : reader.get_vonatok())
    {
        for(auto & all_it : von_it.get_menetrend())
        {
            kaki.insert(all_it.allomas);
        }
        t_max.push_back((*von_it.get_menetrend().end()).ido);
    }
    for(auto & cucc_it : reader.get_cuccok())
    {
        kaki.insert(cucc_it.get_honnan());
        kaki.insert(cucc_it.get_hova());
        //cout<<cucc_it.get_honnan()<<" "<<cucc_it.get_hova()<<endl;
    }
    bool van_kocsi = false;
    for(auto & all_it : kaki)
    {
        //cout<<all_it<<endl;
        for(auto & kocs_it : reader.get_kocsik())
        {
            if(kocs_it.get_allomas() == all_it)
            {
                van_kocsi = true;
                break;
            }
        }
    }
    if(van_kocsi)
    {
        //for(int t = 0; t<terkep.lkkt(t_max); t++)
        //{
        int t = 0;
        for(auto & honnan_it : kaki)
        {
            for(auto & hova_it : kaki)
            {
                bool finish = false;

                map<string, int> opens = {{honnan_it, t}};
                pair<string, int> current_all = *opens.begin();
                while(!finish)
                {
                    if(opens.empty())
                    {
                        for(auto & cucc_it : reader.get_cuccok())
                        {
                            if(cucc_it.get_honnan() == honnan_it && cucc_it.get_hova() == hova_it)
                            {
                                terkep.szomoru.push_back(cucc_it);
                            }
                        }
                        finish = true;
                    }
                    else
                    {
                        current_all = min(opens);
                        //cout<<current_all.first<<" "<<current_all.second<<endl;
                        for(auto & von_it : reader.get_vonatok())
                        {
                            for(int menet_i = 0; menet_i<von_it.get_menetrend().size(); menet_i++)
                            {
                                if(current_all.first == von_it.get_menetrend()[menet_i].allomas){
                                    if(current_all.first == von_it.get_menetrend()[(menet_i+1) % von_it.get_menetrend().size()].allomas)
                                    {
                                        if((current_all.second % von_it.get_menetrend().back().ido) >= (von_it.get_menetrend()[menet_i].ido % von_it.get_menetrend().back().ido) && (current_all.second % von_it.get_menetrend().back().ido) < (von_it.get_menetrend()[(menet_i+2) % von_it.get_menetrend().size()].ido % von_it.get_menetrend().back().ido))
                                        {
                                                if(opens.find((von_it.get_menetrend()[(menet_i+2) % von_it.get_menetrend().size()]).allomas) != opens.end())
                                                {
                                                    if(opens[(von_it.get_menetrend()[(menet_i+2) % von_it.get_menetrend().size()]).allomas] > current_all.second + (von_it.get_menetrend()[(menet_i+2) % von_it.get_menetrend().size()]).ido % von_it.get_menetrend().back().ido - (current_all.second % von_it.get_menetrend().back().ido))
                                                    {
                                                        opens[(von_it.get_menetrend()[(menet_i+2) % von_it.get_menetrend().size()]).allomas] = current_all.second + (von_it.get_menetrend()[(menet_i+2) % von_it.get_menetrend().size()]).ido % von_it.get_menetrend().back().ido - (current_all.second % von_it.get_menetrend().back().ido);
                                                    }
                                                }
                                                else
                                                {
                                                    if(von_it.get_menetrend()[(menet_i+2) % von_it.get_menetrend().size()].allomas == hova_it)
                                                    {
                                                        //cout<<"ok "<<hova_it<<endl;
                                                        finish = true;
                                                        //orakulum[i][honnan_it][hova_it] =
                                                    }
                                                    opens[(von_it.get_menetrend()[(menet_i+2) % von_it.get_menetrend().size()]).allomas] = current_all.second + (von_it.get_menetrend()[(menet_i+2) % von_it.get_menetrend().size()]).ido % von_it.get_menetrend().back().ido - (current_all.second % von_it.get_menetrend().back().ido);
                                                }
                                        }
                                    }
                                    else
                                        {
                                            if((current_all.second % von_it.get_menetrend().back().ido) >= (von_it.get_menetrend()[menet_i].ido % von_it.get_menetrend().back().ido) && (current_all.second % von_it.get_menetrend().back().ido) < (von_it.get_menetrend()[(menet_i+1) % von_it.get_menetrend().size()].ido % von_it.get_menetrend().back().ido))
                                            {
                                                if(opens.find((von_it.get_menetrend()[(menet_i+1) % von_it.get_menetrend().size()]).allomas) != opens.end())
                                                {
                                                    if(opens[(von_it.get_menetrend()[(menet_i+1) % von_it.get_menetrend().size()]).allomas] > current_all.second + (von_it.get_menetrend()[(menet_i+1) % von_it.get_menetrend().size()]).ido % von_it.get_menetrend().back().ido - (current_all.second % von_it.get_menetrend().back().ido))
                                                    {
                                                        opens[(von_it.get_menetrend()[(menet_i+1) % von_it.get_menetrend().size()]).allomas] = current_all.second + (von_it.get_menetrend()[(menet_i+1) % von_it.get_menetrend().size()]).ido % von_it.get_menetrend().back().ido - (current_all.second % von_it.get_menetrend().back().ido);
                                                    }
                                                }
                                                else
                                                {
                                                    if(von_it.get_menetrend()[(menet_i+1) % von_it.get_menetrend().size()].allomas == hova_it)
                                                    {
                                                        //cout<<hova_it<<endl;
                                                        finish = true;
                                                        //orakulum[i][honnan_it][hova_it] =
                                                    }
                                                    opens[(von_it.get_menetrend()[(menet_i+1) % von_it.get_menetrend().size()]).allomas] = current_all.second + (von_it.get_menetrend()[(menet_i+1) % von_it.get_menetrend().size()]).ido % von_it.get_menetrend().back().ido - (current_all.second % von_it.get_menetrend().back().ido);
                                                }
                                            }

                                        }
                                    //cout<<menet_i<<" "<<von_it.get_menetrend()[menet_i].allomas<<" "<<(menet_i+1) % von_it.get_menetrend().size()<<" "<<von_it.get_menetrend()[(menet_i+1) % von_it.get_menetrend().size()].allomas<<endl;

                                }
                            }
                        }
                        opens.erase(current_all.first);
                    }
                }
            }
        }
        //}
    }
    else
    {
        //nagyon szomorú
        for(auto & cucc_it : reader.get_cuccok())
        {
            terkep.szomoru.push_back(cucc_it);
        }
    }
}

int Graph::max(vector<int> puki)
{
    if(puki.empty())
    {
        return 0;
    }
    else
    {
        int maxi = *puki.begin();
        for(auto & pukit : puki)
        {
            if(pukit > maxi)
            {
                maxi = pukit;
            }
        }
        return maxi;
    }
}

int Graph::lkkt(const vector<int>& pisi)
{
    bool nemjo = true;
    int i = 1;
    int j;
    int maxi = max(pisi);
    while(nemjo)
    {
        j = i*maxi;
        nemjo = false;
        for(auto & pisit : pisi)
        {
            if(i % pisit != 0)
            {
                nemjo = true;
                break;
            }
        }
        i++;
    }
    return j;
}

pair<string, int> Graph::min(map<string, int> hulye)
{
    pair<string, int> current_min = *hulye.begin();
    for(auto & hulyeit : hulye)
    {
        if(hulyeit.second < current_min.second)
        {
            current_min = hulyeit;
        }
    }
    return current_min;
}

pair<string, int> Graph::jo_vonat(const int &ido, const string &honnan, const string &hova) {
    pair<string, int> jo_vonat;
    return jo_vonat;
}
