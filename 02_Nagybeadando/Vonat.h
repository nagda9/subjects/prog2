#ifndef NAGYBEADANDO_VONAT_H
#define NAGYBEADANDO_VONAT_H

#include <iostream>
#include <sstream>
#include <vector>

using namespace std;

struct Menetrend
{
    string allomas;
    int ido;
};

class Vonat {
private:
    string nev;
    int kapacitas{};
    vector<Menetrend> menetrend;

public:
    Vonat();
    Vonat(string nev_, int kapacitas_, vector<Menetrend> menetrend_);
    void kiir(Vonat& vonat);
    bool operator<(const Vonat& vonat) const;
    [[nodiscard]] string get_nev() const;
    [[nodiscard]] int get_kapacitas() const;
    [[nodiscard]] vector<Menetrend> get_menetrend() const;
};

#endif //NAGYBEADANDO_VONAT_H
