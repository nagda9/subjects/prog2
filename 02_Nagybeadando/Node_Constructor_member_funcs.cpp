#include "Node.h"
#include "Akcio.h"

void Node::do_actions()
{
    // végrehajtuk az akciótervet
    for (const auto &akcio : akcio_diff) {
        if (akcio.get_feladat() == "felpakol" || akcio.get_feladat() == "lepakol") {
            for (auto node_pair : node) {
                for (const auto &kocsi_parok : node_pair.second.kocsik) {
                    if (kocsi_parok.first.get_nev() == akcio.get_kocsi().get_nev()) {
                        if (akcio.get_feladat() == "felpakol") {
                            if (node_pair.second.cuccok.find(akcio.get_cucc().first) != node_pair.second.cuccok.end()) {
                                if (node[node_pair.first].cuccok[akcio.get_cucc().first] >= akcio.get_cucc().second) {
                                    int sum = 0;
                                    for (const auto &cucc_parok : kocsi_parok.second) {
                                        sum += cucc_parok.second;
                                    }
                                    if (sum + akcio.get_cucc().second <= kocsi_parok.first.get_kapacitas()) {
                                        if (node_pair.second.kocsik[akcio.get_kocsi()].find(akcio.get_cucc().first) ==
                                            node_pair.second.kocsik[akcio.get_kocsi()].end()) {
                                            node[node_pair.first].kocsik[akcio.get_kocsi()].insert(akcio.get_cucc());
                                        } else {
                                            node[node_pair.first].kocsik[akcio.get_kocsi()][akcio.get_cucc().first] += akcio.get_cucc().second;
                                        }
                                        node[node_pair.first].cuccok[akcio.get_cucc().first] -= akcio.get_cucc().second;
                                    } else {
                                        cout << "Akcio hiba: " + akcio.get_feladat() + " " +
                                                akcio.get_kocsi().get_nev() + " " + akcio.get_cucc().first.get_nev() +
                                                " " << akcio.get_cucc().second
                                             << ": Nincs hely a " + kocsi_parok.first.get_nev() +
                                                " kocsin! A szabad kapacitas "
                                             << kocsi_parok.first.get_kapacitas() - sum << endl;
                                    }
                                } else {
                                    cout << "Akcio hiba: " + akcio.get_feladat() + " " + akcio.get_kocsi().get_nev() +
                                            " " + akcio.get_cucc().first.get_nev() + " " << akcio.get_cucc().second
                                         << ": Csak " << node[node_pair.first].cuccok[akcio.get_cucc().first] << " "
                                         << akcio.get_cucc().first.get_nev() + " van az allomason!" << endl;
                                }
                            } else {
                                cout << "Akcio hiba: " + akcio.get_feladat() + " " + akcio.get_kocsi().get_nev() + " " +
                                        akcio.get_cucc().first.get_nev() + " " << akcio.get_cucc().second
                                     << ": Nincs " + akcio.get_cucc().first.get_nev() + " " + node_pair.first +
                                        " allomason!" << endl;
                            }
                        } else if (akcio.get_feladat() == "lepakol") {
                            if (kocsi_parok.second.find(akcio.get_cucc().first) != kocsi_parok.second.end()) {
                                if (akcio.get_cucc().second <=
                                    node[node_pair.first].kocsik[akcio.get_kocsi()][akcio.get_cucc().first]) {
                                    node[node_pair.first].kocsik[akcio.get_kocsi()][akcio.get_cucc().first] -= akcio.get_cucc().second;

                                    if (node[node_pair.first].cuccok.find(akcio.get_cucc().first) ==
                                        node[node_pair.first].cuccok.end()) {
                                        node[node_pair.first].cuccok.insert(akcio.get_cucc());
                                    } else {
                                        node[node_pair.first].cuccok[akcio.get_cucc().first] += akcio.get_cucc().second;
                                    }
                                } else {
                                    cout << "Akcio hiba: " + akcio.get_feladat() + " " + akcio.get_kocsi().get_nev() +
                                            " " + akcio.get_cucc().first.get_nev() + " " << akcio.get_cucc().second
                                         << ": Nincs " << akcio.get_cucc().second << " "
                                         << akcio.get_cucc().first.get_nev() + " " + akcio.get_kocsi().get_nev() +
                                            " kocsin!" << endl;
                                }
                            } else {
                                cout << "Akcio hiba: " + akcio.get_feladat() + " " + akcio.get_kocsi().get_nev() + " " +
                                        akcio.get_cucc().first.get_nev() + " " << akcio.get_cucc().second
                                     << ": Nincs " + akcio.get_cucc().first.get_nev() + " " +
                                        kocsi_parok.first.get_nev() + " kocsin!" << endl;
                            }
                        }

                        if (node[node_pair.first].cuccok[akcio.get_cucc().first] == 0) {
                            node[node_pair.first].cuccok.erase(akcio.get_cucc().first);
                        }

                        if (node[node_pair.first].kocsik[akcio.get_kocsi()][akcio.get_cucc().first] == 0) {
                            node[node_pair.first].kocsik[akcio.get_kocsi()].erase(akcio.get_cucc().first);
                        }
                    }
                }
            }
        }

        if (akcio.get_feladat() == "felcsatol" || akcio.get_feladat() == "lecsatol") {
            for (auto node_pair : node) {
                for (const auto &vonat_parok : node_pair.second.vonatok) {
                    if (vonat_parok.first.get_nev() == akcio.get_vonat().get_nev()) {
                        if (akcio.get_feladat() == "felcsatol") {
                            if (node_pair.second.kocsik.find(akcio.get_kocsi()) != node_pair.second.kocsik.end()) {
                                int sum = 0;
                                for (const auto &kocsi_parok : node[node_pair.first].vonatok[akcio.get_vonat()]) {
                                    sum++;
                                }
                                if (sum < vonat_parok.first.get_kapacitas()) {
                                    for (const auto &kocsi_parok : node_pair.second.kocsik) {
                                        if (kocsi_parok.first.get_nev() == akcio.get_kocsi().get_nev()) {
                                            node[node_pair.first].vonatok[akcio.get_vonat()].insert(kocsi_parok);
                                            node[node_pair.first].kocsik.erase(akcio.get_kocsi());
                                        }
                                    }
                                } else {
                                    cout << "Akcio hiba: " + akcio.get_feladat() + " " + akcio.get_vonat().get_nev() +
                                            " " + akcio.get_kocsi().get_nev()
                                         << ": Nincs hely " + akcio.get_vonat().get_nev() + " vonaton!" << endl;
                                }
                            } else {
                                cout << "Akcio hiba: " + akcio.get_feladat() + " " + akcio.get_vonat().get_nev() + " " +
                                        akcio.get_kocsi().get_nev()
                                     << ": Nincs " + akcio.get_kocsi().get_nev() + " kocsi az allomason!" << endl;
                            }
                        } else if (akcio.get_feladat() == "lecsatol") {
                            if (node_pair.second.vonatok[akcio.get_vonat()].find(akcio.get_kocsi()) !=
                                node_pair.second.vonatok[akcio.get_vonat()].end()) {
                                for (const auto &kocsi_parok : node_pair.second.vonatok[akcio.get_vonat()]) {
                                    if (kocsi_parok.first.get_nev() == akcio.get_kocsi().get_nev()) {
                                        node[node_pair.first].kocsik.insert(kocsi_parok);
                                        node[node_pair.first].vonatok[akcio.get_vonat()].erase(akcio.get_kocsi());
                                    }
                                }
                            } else {
                                cout << "Akcio hiba: " + akcio.get_feladat() + " " + akcio.get_vonat().get_nev() + " " +
                                        akcio.get_kocsi().get_nev()
                                     << ": Nincs " + akcio.get_kocsi().get_nev() + " kocsi a vonathoz csatlakoztatva!"
                                     << endl;
                            }
                        }
                    }
                }
            }
        }
    }
}

void Node::move_train() {
    //mozgatjuk a vonatot
    for (const auto &node_parok : (*parent).node) {
        for (const auto &vonat_parok : node_parok.second.vonatok) {
            for (size_t i = 0; i < vonat_parok.first.get_menetrend().size(); i++) {
                if (vonat_parok.first.get_menetrend()[i].ido % (vonat_parok.first.get_menetrend().back().ido) == ((ido) % (vonat_parok.first.get_menetrend().back().ido))) {
                    if (node_parok.first != vonat_parok.first.get_menetrend()[i].allomas) {
                        node[vonat_parok.first.get_menetrend()[i].allomas].vonatok.insert(
                                {vonat_parok.first, node[node_parok.first].vonatok[vonat_parok.first]});
                        node[node_parok.first].vonatok.erase(vonat_parok.first);
                    }
                }
            }
        }
    }
}

void Node::torol(const string& honnan, Cucc mit) {
    node[honnan].cuccok.erase(mit);
}
