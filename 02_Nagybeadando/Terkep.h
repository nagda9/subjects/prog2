#ifndef NAGYBEADANDO_TERKEP_H
#define NAGYBEADANDO_TERKEP_H

#include <map>
#include <vector>
#include <set>
#include "Reader.h"

using namespace std;

class Terkep {
private:

    static vector<map<string,map<string, pair<string, int>>>> orakulum;
    set<pair<string,string>> nem_lehet;
    set<Cucc> szomoru;

public:
    Terkep();
    explicit Terkep(Reader& reader);
    static pair<string, int> jo_vonat (const int& ido, const string& honnan, const string& hova);
};


#endif //NAGYBEADANDO_TERKEP_H
