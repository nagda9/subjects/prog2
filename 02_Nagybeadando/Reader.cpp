#include "Reader.h"

void Reader::check_file(ifstream& bf)
{
    if (!bf.good())
    {
        cerr << "Could not open file!";
    }
}

Reader::Reader(ifstream& bf)
{
    check_file(bf);

    string line;
    while (bf.good())
    {
        getline(bf, line);
        if (!line.rfind("Menetrend", 0))
        {
            getline(bf, line);
            string nev;
            int kapacitas;
            stringstream(line) >> nev >> kapacitas;

            vector<Menetrend> menetrend;
            getline(bf, line);
            while(!line.empty() && bf.good())
            {
                Menetrend allomas;
                stringstream(line) >> allomas.allomas >> allomas.ido;
                menetrend.push_back(allomas);
                getline(bf, line);
            }
            Vonat vonat(nev, kapacitas, menetrend);
            vonatok.push_back(vonat);
        }
        else if (!line.rfind("Kocsik", 0))
        {
            getline(bf, line);
            while(!line.empty() && bf.good())
            {
                string nev;
                int kapacitas;
                string allomas;

                stringstream(line) >> nev >> kapacitas >> allomas;

                Kocsi kocsi(nev, kapacitas, allomas);
                kocsik.push_back(kocsi);
                getline(bf, line);
            }
        }
        else if (!line.rfind("Cuccok", 0))
        {
            getline(bf, line);
            while(!line.empty() && bf.good())
            {
                string nev, honnan, hova;
                int mennyiseg;

                stringstream(line) >> nev >> honnan >> hova >> mennyiseg;

                Cucc cucc(nev, honnan, hova, mennyiseg);
                cuccok.push_back(cucc);
                getline(bf, line);
            }
        }
    }
}

vector<Vonat> Reader::get_vonatok() const {
    return vonatok;
}

vector<Kocsi> Reader::get_kocsik()
{
    return kocsik;
}

vector<Cucc> Reader::get_cuccok()
{
    return cuccok;
}
