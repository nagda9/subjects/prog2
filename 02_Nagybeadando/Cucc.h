#ifndef NAGYBEADANDO_CUCC_H
#define NAGYBEADANDO_CUCC_H

#include <iostream>
#include <sstream>

using namespace std;

class Cucc {
private:
    string nev;
    string honnan;
    string hova;
    int mennyiseg;

public:
    Cucc();
    Cucc(string nev_, string honnan_, string hova_, int mennyiseg_);
    bool operator< (const Cucc& cucc) const;
    void kiir(Cucc& cucc);
    [[nodiscard]] string get_nev() const;
    [[nodiscard]] string get_honnan() const;
    [[nodiscard]] string get_hova() const;
    [[nodiscard]] int get_mennyiseg() const;
    bool operator==(const Cucc& rhs);
};


#endif //NAGYBEADANDO_CUCC_H
