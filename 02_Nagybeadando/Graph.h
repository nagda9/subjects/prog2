#ifndef NAGYBEADANDO_GRAPH_H
#define NAGYBEADANDO_GRAPH_H

#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <set>
#include <list>
#include "Node.h"

using namespace std;

struct Terkep
{
    static vector<map<string,map<string, pair<string, int>>>> orakulum;
    list<Cucc> szomoru;
};

class Graph {
private:
    list<Node *> graph;
    list<Node **> open;
    int chunky;
    Terkep terkep;

public:
    explicit Graph(Node * parent_node, const Reader& reader);

    void create_children(list<Node *>& children, Node * parent_node);
    void lepakol(Node *parent_node, vector<vector<Akcio>>& akciok);
    void felpakol(Node *parent_node, vector<vector<Akcio>>& akciok);
    static void lecsatol(Node *parent_node, vector<vector<Akcio>>& akciok);
    static void felcsatol(Node *parent_node, vector<vector<Akcio>>& akciok);

    list<Node **>::iterator min_open();
    static list<Node *>::iterator ellenoriz(list<Node *>& nodes);
    static int heur(Node * node);

    void create_terkep (Reader reader);
    static pair<string, int> jo_vonat (const int& ido, const string& honnan, const string& hova);
    int max(vector<int> puki);
    int lkkt(const vector<int>& pisi);
    pair<string, int> min(map<string, int> hulye);
};


#endif //NAGYBEADANDO_GRAPH_H
