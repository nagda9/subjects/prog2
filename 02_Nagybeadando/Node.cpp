#include "Node.h"

Node::Node() = default;

Node::~Node()
{
    delete parent;
}

Node::Node(Reader& reader)
{
    set<string> allomas_nev;
    for(const Vonat& v : reader.get_vonatok())
    {
        for (auto& m : v.get_menetrend())
        {
            allomas_nev.insert(m.allomas);
        }
    }
    for(auto & cucc : reader.get_cuccok())
    {
        allomas_nev.insert(cucc.get_honnan());
        allomas_nev.insert(cucc.get_hova());
    }

    for (const auto& s : allomas_nev)
    {
        Allomas allomas;
        for (const Vonat& v : reader.get_vonatok())
        {
            if (v.get_menetrend()[0].allomas == s)
            {
                allomas.vonatok.insert(pair<Vonat, map<Kocsi, map<Cucc,int>>>(v, map<Kocsi, map<Cucc,int>>()));
            }
        }
        for (const Kocsi& k : reader.get_kocsik())
        {
            if (k.get_allomas() == s)
            {
                allomas.kocsik.insert(pair<Kocsi, map<Cucc,int>>(k, map<Cucc,int>()));
            }
        }
        for (const Cucc& c : reader.get_cuccok())
        {
            if (c.get_honnan() == s)
            {
                allomas.cuccok.insert(pair<Cucc, int>(c, c.get_mennyiseg()));
            }
        }
        node[s] = allomas;
    }
    pontszam = 0;
    ido = 0;
    parent = nullptr;
    akcio_diff = {Akcio()};
}

Node::Node(Node* parent_node, const vector<Akcio>& akciok)
{
    node = parent_node->node;
    ido = parent_node->ido +1;
    parent = parent_node;
    akcio_diff = akciok;

    do_actions();
    move_train();
}

void Node::kiir()
{
    akcio_kiir();
    cout << endl;

    for (const auto& node_parok : node)
    {
        cout << node_parok.first << endl;
        for (const auto& vonat_parok : node_parok.second.vonatok)
        {
            cout << "  " << vonat_parok.first.get_nev();
            for (const auto& kocsi_parok : vonat_parok.second)
            {
                cout << endl;
                cout << "  " << "|-> " << kocsi_parok.first.get_nev();
                for (const auto& cucc_parok : kocsi_parok.second)
                {
                    cout << endl;
                    cout << "      " << "|-> "<< cucc_parok.first.get_nev() << " " << cucc_parok.second;
                }
            }
            cout << endl;
        }
        for (const auto& kocsik : node_parok.second.kocsik)
        {
            cout << "  " << kocsik.first.get_nev() << " ";
            for (const auto& cucc_parok : kocsik.second)
            {
                cout << endl;
                cout << "  " << "|-> " << cucc_parok.first.get_nev() << " " << cucc_parok.second;
            }
            cout << endl;
        }
        for (const auto& cucc_parok : node_parok.second.cuccok)
        {
            cout << "  " << cucc_parok.first.get_nev() << " " << cucc_parok.second << endl;
        }
    }
    cout<<endl;
    cout << "------------------------" << endl;
}

void Node::akcio_kiir()
{
    for (auto akcio : akcio_diff)
    {
        cout << ido-1 << " ";
        akcio.kiir();
    }
}

int Node::get_pontszam()
{
    return pontszam;
}

void Node::set_pontszam(int pont)
{
    pontszam = pont;
}

int Node::get_ido()
{
    return ido;
}

Node * Node::get_parent()
{
    return parent;
}

map<string, Allomas> Node::get_node()
{
    return node;
}

bool Node::is_finish() {
    for (auto &node_parok : node)
    {
        for(auto & vonat_parok : node_parok.second.vonatok)
        {
            for(auto & kocsi_parok : vonat_parok.second)
            {
                if(!kocsi_parok.second.empty())
                {
                    return false;
                }
            }
        }
    }
    for (auto &node_parok : node)
    {
            for(auto & kocsi_parok : node_parok.second.kocsik)
            {
                if(!kocsi_parok.second.empty())
                {
                    return false;
                }
            }
    }
    for (const auto& node_parok : node)
    {
        for (const auto& cucc_parok: node_parok.second.cuccok)
        {
            if (node_parok.first!=cucc_parok.first.get_hova() || cucc_parok.first.get_mennyiseg()!=cucc_parok.second)
            {

                return false;
            }
        }
    }
    return true;
}
