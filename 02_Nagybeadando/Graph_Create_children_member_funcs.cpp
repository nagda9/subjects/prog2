#include "Graph.h"

void Graph::lecsatol(Node * parent_node, vector<vector<Akcio>>& akciok)
{
    for(auto & all_it : (*parent_node).get_node()) { // kocsik lecsatolása
        for (auto & von_it : all_it.second.vonatok) {
            for (auto & kocs_it : von_it.second)
            {
                vector<vector<Akcio>> ujv;
                for (auto & it : akciok) // összes lehetőség legyártása
                {
                    vector<Akcio> uj_akc = it;
                    uj_akc.emplace_back("lecsatol", von_it.first, kocs_it.first);
                    ujv.push_back(uj_akc);
                }
                akciok.insert(akciok.begin(), ujv.begin(), ujv.end()); // így az is megmarad amiben nem lett lecsatolva
            }
        }
    }
}

void Graph::lepakol(Node * parent_node, vector<vector<Akcio>>& akciok)
{
    for(auto & all_it : (*parent_node).get_node()) { // alapból állomáson álló kocsikból lepakolás
        for (auto &kocs_it : all_it.second.kocsik)
        {
            for (auto &cucc_it : kocs_it.second) // nem kell esetre bontani, mert a lepakolás biztos, hogy előbb van
            {
                vector<vector<Akcio>> ujv;
                for (auto & it : akciok) // összes lehetőség legyártása
                {
                    for (int i = 1; i <= (int)(ceil(((double)cucc_it.second)/chunky)); i++) // i-t veszünk le egy cuccból
                    {
                        vector<Akcio> uj_akc = it;
                        uj_akc.emplace_back("lepakol", kocs_it.first, pair<Cucc, int>(cucc_it.first, fmin(chunky*(double)i,(double)cucc_it.second)));
                        ujv.push_back(uj_akc);
                    }
                }
                akciok.insert(akciok.begin(), ujv.begin(), ujv.end());
            }
        }
    }
    /*
    vector<vector<Akcio>> v;// esetenként kell nézni, mert nem mindegyik szálban lehet mindent
    for(auto & it : akciok) // lecsatolt kocsikból lepakolás
    {
        vector<vector<Akcio>> ujv = {it}; // Ehhez az ághoz lesz legyártva az összes kombináció
        list<Kocsi> kocsii;
        for (auto &akc_it : it) {
            if (akc_it.get_feladat() == "lecsatol") // az adott akcióban le lett csatolva egy kocsi
            {
                kocsii.push_back(akc_it.get_kocsi());
            }
        }
        for (auto &benakocs_it :kocsii) {
            for (auto &all_it : (*parent_node).get_node()) { //végigmegyek a kocsikon a node-ban, mivel a Allomas-on belül akarom kezelni
                for (auto &von_it : all_it.second.vonatok) {
                    for (auto &kocs_it : von_it.second) {
                        if (benakocs_it == kocs_it.first) { // ha ahhoz jutok, ami le lett csatolva
                            for (auto &cucc_it : kocs_it.second) { // végigmegyek a cuccokon
                                vector<vector<Akcio>> uujv;
                                for (auto &uvit : ujv) {
                                    for (int i = 1; i <= (int) (ceil(((double) cucc_it.second) / 5)); i++) // i-t veszünk le egy cuccból{
                                    {
                                        vector<Akcio> ujak = uvit;
                                        ujak.emplace_back("lepakol", kocs_it.first, pair<Cucc, int>(cucc_it.first,fmin(5 * (double)i, (double)cucc_it.second)));
                                        uujv.push_back(ujak); // létrehozom a variációkat erre az akcióra
                                    }
                                }
                                ujv.insert(ujv.begin(), uujv.begin(),
                                           uujv.end()); // csak ezeket az akciókat fűzöm hozzá
                            } // ezután ezzel a cuccal ebben a kocsiban nem foglalkozom
                        } // viszont a többi cuccot lehet pakolni ugyan ezekben az akciókban, ugyanebben a kocsiban
                    }
                }
            }
        }

        v.insert(v.begin(), ujv.begin(),ujv.end()); // insert-el és azokkal már nem kel foglalkozni
    }
    akciok = v;*/
}

void Graph::felpakol(Node * parent_node, vector<vector<Akcio>>& akciok)
{
    // a már állásokon lévő kocsikba felpakolás
    for(auto & all_it : (*parent_node).get_node()) {
        for (auto &kocs_it : all_it.second.kocsik) {
            for (auto &cucc_it : all_it.second.cuccok) { // már lent lévő cuccokból pakolunk
                vector<vector<Akcio>> ujv;
                for (auto & it : akciok) { // szálanként kell nézni
                    int ferohely = kocs_it.first.get_kapacitas();
                    for (auto &c_it : kocs_it.second)  //mennyit lehet az adott kocsiba pakolni alapból
                    {
                        ferohely -= c_it.second;
                    }
                    int van = cucc_it.second; // már cuccokra bontottuk, tehát elég csak egy szám
                    bool lehet= true;
                    for (auto &akc_it : it) //adott akciósorban mennyi van az állomáson iletve mennyi fér el
                    {
                        if (akc_it.get_feladat() == "lepakol" )
                        {
                            if(akc_it.get_cucc().first == cucc_it.first)
                            {
                                if(akc_it.get_kocsi() == kocs_it.first) // ha ezt ebből már lepakoltuk ,akkor vissza ne
                                {
                                    lehet = false;
                                    break;
                                }
                                // ha az adott állomáson lett lepakolva
                                /*if(all_it.second.kocsik.find(akc_it.get_kocsi()) != all_it.second.kocsik.end())
                                {
                                    van += akc_it.get_cucc().second;
                                }*/
                            }
                            // ha a kiszemelt kocsiból lett hely felszabadítva
                            /*if(akc_it.get_kocsi() == kocs_it.first)
                            {
                                ferohely += akc_it.get_cucc().second;
                            }*/
                        }
                        if (akc_it.get_feladat() == "felpakol")
                        {
                            if(akc_it.get_cucc().first == cucc_it.first)
                            {
                                if(akc_it.get_kocsi() == kocs_it.first)
                                {
                                    lehet = false;
                                    break;
                                }
                                // ha az adott állomáson lett felpakolva
                                if(all_it.second.kocsik.find(akc_it.get_kocsi()) != all_it.second.kocsik.end())
                                {
                                    van -= akc_it.get_cucc().second;
                                }
                            }
                            // ha a kiszemelt kocsiból lett hely elfoglalva
                            if(akc_it.get_kocsi() == kocs_it.first)
                            {
                                ferohely -= akc_it.get_cucc().second;
                            }
                        }
                    }
                    if(lehet)
                    {
                        for (int i = 1;
                             i <= (int)(ceil(((double)van)/chunky)) && i <= (int)(ceil(((double)ferohely)/chunky)); i++) //annyit lehet felpakolni, amennyi van, illetve amennyi felfér
                        {
                            vector<Akcio> ujak = it;
                            ujak.emplace_back("felpakol", kocs_it.first, pair<Cucc, int>(cucc_it.first, fmin(fmin(chunky*(double)i,(double)van),(double)ferohely)));
                            ujv.push_back(ujak);
                        }
                    }
                }
                akciok.insert(akciok.begin(),ujv.begin(),ujv.end());
                // na a lényeg, hogy cucconként kell halmozni
            }
            /*
            vector<vector<Akcio>> v;
            for(auto & it : akciok) // azokból pakolunk, amik alapból nem voltak az állomáson
            {
                vector<vector<Akcio>> ujv = {it};
                map<Cucc, int> cucci; // az adott akcióban így elérhető cuccok összegyűjtése
                for(auto & akc_it : it) {
                    if (akc_it.get_feladat() == "lepakol" &&
                        all_it.second.kocsik.find(akc_it.get_kocsi()) != all_it.second.kocsik.end() &&
                        all_it.second.cuccok.find(akc_it.get_cucc().first) ==
                        all_it.second.cuccok.end()) // ha lepakolunk, és az állomáson vagyunk, de nem volt alapból ilyen cucc
                    {
                        cucci[akc_it.get_cucc().first] += akc_it.get_cucc().second;
                    }
                }
                for(auto & cucc_it : cucci) // az összegyűjtött cuccok szerint megyünk
                {
                    vector<vector<Akcio>> uujv;
                    for(auto & uvit :ujv)
                    {
                        int ferohely = kocs_it.first.get_kapacitas();
                        for (auto &c_it : kocs_it.second)  //mennyit lehet az adott kocsiba pakolni alapból
                        {
                            ferohely -= c_it.second;
                        }
                        int van = cucc_it.second; // már cucconként nézzük, tehát elég egy szám
                        bool lehet = true;
                        for (auto &akc_it : uvit) //adott akciósorban mennyi van az állomáson iletve mennyi fér el
                        {
                            if (akc_it.get_feladat() == "lepakol") {
                                if (akc_it.get_cucc().first == cucc_it.first) {
                                    if(akc_it.get_kocsi() == kocs_it.first)
                                    {
                                        lehet = false;
                                        break;
                                    }
                                    // ha az adott állomáson lett lepakolva
                                    if (all_it.second.kocsik.find(akc_it.get_kocsi()) != all_it.second.kocsik.end()) {
                                        van += akc_it.get_cucc().second;
                                    }
                                }
                                // ha a kiszemelt kocsiból lett hely felszabadítva
                                if (akc_it.get_kocsi() == kocs_it.first) {
                                    ferohely += akc_it.get_cucc().second;
                                }
                            }
                            if (akc_it.get_feladat() == "felpakol") {
                                if (akc_it.get_cucc().first == cucc_it.first) {
                                    if(akc_it.get_kocsi() == kocs_it.first)
                                    {
                                        lehet = false;
                                        break;
                                    }
                                    // ha az adott állomáson lett felpakolva
                                    if (all_it.second.kocsik.find(akc_it.get_kocsi()) != all_it.second.kocsik.end()) {
                                        van -= akc_it.get_cucc().second;
                                    }
                                }
                                // ha a kiszemelt kocsiból lett hely elfoglalva
                                if (akc_it.get_kocsi() == kocs_it.first) {
                                    ferohely -= akc_it.get_cucc().second;
                                }
                            }
                        }
                        if(lehet)
                        {

                            for (int i = 1;
                                 i <= (int)(ceil(((double)van)/5)) &&
                                 i <= (int)(ceil(((double)ferohely)/5)); i++) //annyit lehet felpakolni, amennyi van, illetve amennyi felfér
                            {
                                vector<Akcio> ujak = uvit;
                                ujak.emplace_back("felpakol", kocs_it.first,
                                                  pair<Cucc, int>(cucc_it.first, fmin(fmin(5 * (double)i, (double)van), (double)ferohely)));
                                uujv.push_back(ujak);
                            }
                        }
                    }
                    ujv.insert(ujv.begin(),uujv.begin(),uujv.end());
                }
                v.insert(v.begin(), ujv.begin(), ujv.end());
            }
            akciok=v;*/
        }
    }


    vector<vector<Akcio>> v = {{}};
    for(auto & it : akciok) // lecsatolt kocsikba felpakolás
    {
        list<Kocsi> kocsii;
        bool vankocs = false;
        for (auto &akc_it : it) {
            if (akc_it.get_feladat() == "lecsatol") { //lecsatolt kocsik
                kocsii.push_back(akc_it.get_kocsi());
                vankocs = true;
            }
            if (akc_it.get_feladat() == "felcsatol")
            {
                for(auto lekocs_it = kocsii.begin(); lekocs_it != kocsii.end(); lekocs_it++)
                {
                    if(*lekocs_it == akc_it.get_kocsi())
                    {
                        kocsii.erase(lekocs_it);
                    }
                }

            }
        }
        vector<vector<Akcio>> ujv = {it};
        if(vankocs)
        {
            for (auto &benakocs_it : kocsii) {
                for (auto &all_it : (*parent_node).get_node()) {
                    for (auto &von_it : all_it.second.vonatok) {
                        for(auto & kocs_it : von_it.second)
                        {
                            if (benakocs_it == kocs_it.first) { // kocsinként
                                for (auto &cucc_it : all_it.second.cuccok) { // már lent lévő cuccokból pakolunk
                                    vector<vector<Akcio>> uujv;
                                    for (auto &uvit : ujv) {
                                        int ferohely = kocs_it.first.get_kapacitas();
                                        for (auto &c_it : kocs_it.second)  //mennyit lehet az adott kocsiba pakolni alapból
                                        {
                                            ferohely -= c_it.second;
                                        }
                                        int van = cucc_it.second; // már cucconként nézzük
                                        bool lehet = true;
                                        for (auto &akc_it : uvit) //adott akciósorban mennyi van az állomáson iletve mennyi fér el
                                        {
                                            if (akc_it.get_feladat() == "lepakol") {
                                                if (akc_it.get_cucc().first == cucc_it.first) {
                                                    /*if (akc_it.get_kocsi() == kocs_it.first) {
                                                        lehet = false;
                                                        break;
                                                    }*/
                                                    // ha az adott állomáson lett lepakolva
                                                    if (all_it.second.kocsik.find(akc_it.get_kocsi()) !=
                                                        all_it.second.kocsik.end()) {
                                                        van += akc_it.get_cucc().second;
                                                    }
                                                }
                                                // ha a kiszemelt kocsiból lett hely felszabadítva
                                                /*if (akc_it.get_kocsi() == kocs_it.first) {
                                                    ferohely += akc_it.get_cucc().second;
                                                }*/
                                            }
                                            if (akc_it.get_feladat() == "felpakol") {
                                                if (akc_it.get_cucc().first == cucc_it.first) {
                                                    // ha az adott állomáson lett felpakolva
                                                    if (all_it.second.kocsik.find(akc_it.get_kocsi()) !=
                                                        all_it.second.kocsik.end()) {
                                                        van -= akc_it.get_cucc().second;
                                                    }
                                                    bool talal = false;
                                                    for(auto & kocsikeres : kocsii)
                                                    {
                                                        if(akc_it.get_kocsi() == kocsikeres)
                                                        {
                                                            talal = true;
                                                            break;
                                                        }
                                                    }
                                                    if(talal)
                                                    {
                                                        van -= akc_it.get_cucc().second;
                                                    }
                                                }
                                                // ha a kiszemelt kocsiból lett hely elfoglalva
                                                if (akc_it.get_kocsi() == kocs_it.first) {
                                                    ferohely -= akc_it.get_cucc().second;
                                                }
                                            }
                                        }
                                        if (lehet) {
                                            for (int i = 1;
                                                 i <= (int) (ceil(((double) van) / chunky)) &&
                                                 i <= (int) (ceil(((double) ferohely) /
                                                                  chunky)); i++) //annyit lehet felpakolni, amennyi van, illetve amennyi felfér
                                            {
                                                vector<Akcio> ujak = uvit;
                                                ujak.emplace_back("felpakol", kocs_it.first,
                                                                  pair<Cucc, int>(cucc_it.first,
                                                                                  fmin(fmin(chunky* i, van), ferohely)));
                                                uujv.push_back(ujak);
                                            }
                                        }
                                    }
                                    ujv.insert(ujv.begin(), uujv.begin(), uujv.end());
                                }
                                // itt adjuk hozzá, mivel minden akcióhoz külön kell legyártani, amit az alapból lehet
                                // na a lényeg, hogy cucconként kell halmozni

                                // azokból pakolunk, amik alapból nem voltak az állomáson
                                /*vector<vector<Akcio>> vv;
                                for(auto & uvit : ujv) {
                                    vector<vector<Akcio>> uujv = {uvit};
                                    map<Cucc, int> cucci;
                                    for (auto &akc_it : uvit) {
                                        if (akc_it.get_feladat() == "lepakol") {
                                            for (auto &all_it2 : (*parent_node).get_node()) {
                                                if (all_it2.second.kocsik.find(akc_it.get_kocsi()) !=
                                                    all_it2.second.kocsik.end() &&
                                                    all_it2.second.cuccok.find(akc_it.get_cucc().first) ==
                                                    all_it2.second.cuccok.end()) // ha lepakolunk, és az állomáson vagyunk, de nem volt alapból ilyen cucc
                                                {
                                                    cucci[akc_it.get_cucc().first] += akc_it.get_cucc().second;
                                                }
                                            }
                                        }
                                    }
                                    for (auto &cucc_it : cucci) // az összegyűjtött cuccok szerint megyünk
                                    {
                                        vector<vector<Akcio>> uuujv;
                                        for (auto &uuvit : uujv) {
                                            int ferohely = kocs_it.first.get_kapacitas();
                                            for (auto &c_it : kocs_it.second)  //mennyit lehet az adott kocsiba pakolni alapból
                                            {
                                                ferohely -= c_it.second;
                                            }
                                            int van = cucc_it.second;
                                            bool lehet = true;
                                            for (auto &akc_it : uvit) //adott akciósorban mennyi van az állomáson iletve mennyi fér el
                                            {
                                                if (akc_it.get_feladat() == "lepakol") {
                                                    if (akc_it.get_cucc().first == cucc_it.first) {
                                                        if (akc_it.get_kocsi() == kocs_it.first) {
                                                            lehet = false;
                                                            break;
                                                        }
                                                        // ha az adott állomáson lett lepakolva
                                                        if (all_it.second.kocsik.find(akc_it.get_kocsi()) !=
                                                            all_it.second.kocsik.end()) {
                                                            van += akc_it.get_cucc().second;
                                                        }
                                                    }
                                                    // ha a kiszemelt kocsiból lett hely felszabadítva
                                                    if (akc_it.get_kocsi() == kocs_it.first) {
                                                        ferohely += akc_it.get_cucc().second;
                                                    }
                                                }
                                                if (akc_it.get_feladat() == "felpakol") {
                                                    if (akc_it.get_cucc().first == cucc_it.first) {
                                                        // ha az adott állomáson lett felpakolva
                                                        if (all_it.second.kocsik.find(akc_it.get_kocsi()) !=
                                                            all_it.second.kocsik.end()) {
                                                            van -= akc_it.get_cucc().second;
                                                        }
                                                    }
                                                    // ha a kiszemelt kocsiból lett hely elfoglalva
                                                    if (akc_it.get_kocsi() == kocs_it.first) {
                                                        ferohely -= akc_it.get_cucc().second;
                                                    }
                                                }
                                            }
                                            if (lehet) {
                                                for (int i = 1;
                                                     i <= (int) (ceil(((double) van) / 5)) &&
                                                     i <= (int) (ceil(((double) ferohely) /
                                                                      5)); i++) //annyit lehet felpakolni, amennyi van, illetve amennyi felfér
                                                {
                                                    vector<Akcio> ujak = uuvit;
                                                    ujak.emplace_back("felpakol", kocs_it.first,
                                                                      pair<Cucc, int>(cucc_it.first,
                                                                                      fmin(fmin(5 * i, van), ferohely)));
                                                    uuujv.push_back(ujak);
                                                    //cucconként, akciósoronként halmozunk
                                                }
                                            }
                                        }
                                        uujv.insert(uujv.begin(), uuujv.begin(), uuujv.end());
                                        // itt ért véget a v-n iteráció
                                    }
                                    vv.insert(vv.begin(), uujv.begin(), uujv.end()-1);
                                }
                                ujv.insert(ujv.begin(),vv.begin(),vv.end());*/
                            }
                        }
                    }
                }
            }
        }
        v.insert(v.begin(), ujv.begin(), ujv.end());
    }
    v.erase(v.end()-1);
    akciok=v;

}

void Graph::felcsatol(Node * parent_node, vector<vector<Akcio>>& akciok)
{
    // alapból állomáson lévő kocsik felcsatolása
    for(auto & all_it : (*parent_node).get_node())
    {
        for(auto & von_it : all_it.second.vonatok)
        {
            for(auto & kocs_it : all_it.second.kocsik)
            {
                int ferohely = von_it.first.get_kapacitas();
                for (auto &k_it : von_it.second)  //mennyit lehet az adott vonatba pakolni alapból
                {
                    ferohely--;
                }
                vector<vector<Akcio>> ujv;
                for (auto &vit : akciok) {
                    bool lehet=true;
                    for (auto &akc_it : vit) //adott akciósorban lett-e felpakolva
                    {
                        if ((akc_it.get_feladat() == "felpakol" || akc_it.get_feladat() == "lepakol") && akc_it.get_kocsi() == kocs_it.first) {
                            lehet=false;
                            break;
                        }
                        if (akc_it.get_feladat() == "lecsatol" && von_it.first.get_nev() == akc_it.get_vonat().get_nev())
                        {
                            if(akc_it.get_kocsi() == kocs_it.first && akc_it.get_vonat().get_nev() == von_it.first.get_nev())
                            {
                                lehet=false;
                                break;
                            }
                            ferohely++;
                        }
                        if (akc_it.get_feladat() == "felcsatol"){
                            if(akc_it.get_kocsi() == kocs_it.first)
                            {
                                lehet=false;
                            }
                            if(von_it.first.get_nev() == akc_it.get_vonat().get_nev())
                            {
                                ferohely--;
                            }
                        }
                    }
                    if(lehet && ferohely > 0)
                    {
                        vector<Akcio> ujak = vit;
                        ujak.emplace_back("felcsatol", von_it.first, kocs_it.first);
                        ujv.push_back(ujak);
                    }
                }
                akciok.insert(akciok.begin(), ujv.begin(), ujv.end());
            }
        }
    }



    // közben lecsatolt kocsik felcsatolása
    vector<vector<Akcio>> v = {{}};
    for(auto & it : akciok)
    {
        list<Kocsi> kocsii;
        for(auto & akc_it : it) {
            if (akc_it.get_feladat() == "lecsatol") {
                kocsii.push_back(akc_it.get_kocsi());
            }
            if (akc_it.get_feladat() == "felcsatol")
            {
                for(auto lekocs_it = kocsii.begin(); lekocs_it != kocsii.end(); lekocs_it++)
                {
                    if(*lekocs_it == akc_it.get_kocsi())
                    {
                        kocsii.erase(lekocs_it);
                    }
                }

            }
        }
        vector<vector<Akcio>> ujv = {it};
        for(auto & benakocs_it : kocsii)
        {
            for (auto &all_it : (*parent_node).get_node()) {
                for (auto &von_it : all_it.second.vonatok) {
                    for (auto & kocs_it : von_it.second) {
                        if (benakocs_it == kocs_it.first) {
                            int ferohely = von_it.first.get_kapacitas();
                            for (auto &k_it : von_it.second)  //mennyit lehet az adott vonatba pakolni alapból
                            {
                                ferohely--;
                            }
                            vector<vector<Akcio>> uujv;
                            for (auto & uvit : ujv) {
                                bool lehet=true;
                                for (auto &akc_it : uvit) //adott akciósorban lett-e felpakolva
                                {
                                    if ((akc_it.get_feladat() == "felpakol" || akc_it.get_feladat() == "lepakol") && akc_it.get_kocsi() == kocs_it.first) {
                                        lehet=false;
                                        break;
                                    }
                                    if (akc_it.get_feladat() == "lecsatol" && von_it.first.get_nev() == akc_it.get_vonat().get_nev())
                                    {
                                        if(akc_it.get_kocsi() == kocs_it.first && akc_it.get_vonat().get_nev() == von_it.first.get_nev())
                                        {
                                            lehet=false;
                                            break;
                                        }
                                        ferohely++;
                                    }
                                    if (akc_it.get_feladat() == "felcsatol")
                                    {
                                        if(akc_it.get_kocsi() == kocs_it.first)
                                        {
                                            lehet=false;
                                        }
                                        if(von_it.first.get_nev() == akc_it.get_vonat().get_nev())
                                        {
                                            ferohely--;
                                        }
                                    }
                                }
                                if(lehet && ferohely > 0)
                                {
                                    vector<Akcio> ujak = uvit;
                                    ujak.emplace_back("felcsatol", von_it.first, kocs_it.first);
                                    uujv.push_back(ujak);
                                }
                            }
                            ujv.insert(ujv.begin(), uujv.begin(), uujv.end());
                        }
                    }
                }
            }
        }
        v.insert(v.begin(),ujv.begin(),ujv.end());
    }
    v.erase(v.end()-1);
    akciok=v;
}
