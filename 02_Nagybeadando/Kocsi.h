#ifndef NAGYBEADANDO_KOCSI_H
#define NAGYBEADANDO_KOCSI_H

#include <iostream>
#include <sstream>

using namespace std;

class Kocsi {
private:
    string nev;
    int kapacitas{};
    string allomas;

public:
    Kocsi();
    Kocsi(string nev_, int kapacitas_, string allomas_);
    bool operator<(const Kocsi& kocsi) const;
    bool operator==(Kocsi rhs);
    void kiir(Kocsi& kocsi);
    [[nodiscard]] string get_nev() const;
    [[nodiscard]] int get_kapacitas() const;
    [[nodiscard]] string get_allomas() const;
};


#endif //NAGYBEADANDO_KOCSI_H
