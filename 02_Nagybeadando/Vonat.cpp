#include "Vonat.h"

#include <utility>

Vonat::Vonat() = default;

Vonat::Vonat(string nev_, int kapacitas_, vector<Menetrend> menetrend_) : nev(std::move(nev_)), kapacitas(kapacitas_), menetrend(std::move(menetrend_)) {}

bool Vonat::operator<(const Vonat& vonat) const
{
    return vonat.nev < nev;
}

void Vonat::kiir(Vonat& vonat)
{
    cout << nev << " " << kapacitas << " " << vonat.menetrend[0].allomas << endl;
}

string Vonat::get_nev() const
{
    return nev;
}

int Vonat::get_kapacitas() const
{
    return kapacitas;
}

vector<Menetrend> Vonat::get_menetrend() const
{
    return menetrend;
}
