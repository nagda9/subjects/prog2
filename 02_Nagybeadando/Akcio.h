#ifndef NAGYBEADANDO_AKCIO_H
#define NAGYBEADANDO_AKCIO_H

#include <iostream>
#include <sstream>
#include <map>
#include <vector>
#include <set>
#include <list>
#include "Cucc.h"
#include "Kocsi.h"
#include "Vonat.h"

using namespace std;

class Akcio {
private:
    string feladat; // 4 érték lehet: felpakol, lepakol, felcsatol, lecsatol
    Kocsi kocsi;
    Vonat vonat;
    pair<Cucc, int> cucc;
public:
    Akcio(string feladat_, Kocsi kocsi_, pair<Cucc, int> cucc_);
    Akcio(string feladat_, Vonat vonat_, Kocsi kocsi_);
    Akcio();
    void kiir();
    string get_feladat() const;
    Kocsi get_kocsi() const;
    Vonat get_vonat() const;
    pair<Cucc, int> get_cucc() const;
};


#endif //NAGYBEADANDO_AKCIO_H
