#include "Cucc.h"

#include <utility>

Cucc::Cucc() = default;

Cucc::Cucc(string nev_, string honnan_, string hova_, int mennyiseg_) : nev(std::move(nev_)), honnan(std::move(honnan_)), hova(std::move(hova_)), mennyiseg(mennyiseg_) {}

bool Cucc::operator< (const Cucc& cucc) const
{
    return nev < cucc.nev;
}

void Cucc::kiir(Cucc& cucc)
{
    cout << nev << " " << honnan << " " << hova << " " << mennyiseg << endl;
}

string Cucc::get_nev() const
{
    return nev;
}

string Cucc::get_honnan() const
{
    return honnan;
}

string Cucc::get_hova() const
{
    return hova;
}

int Cucc::get_mennyiseg() const
{
    return mennyiseg;
}

bool Cucc::operator==(const Cucc& rhs)
{
    return nev==rhs.nev;
}