#include "Akcio.h"

Akcio::Akcio(string feladat_, Kocsi kocsi_, pair<Cucc, int> cucc_) : feladat(std::move(feladat_)), kocsi(std::move(kocsi_)), cucc(std::move(cucc_)) {}

Akcio::Akcio(string feladat_, Vonat vonat_, Kocsi kocsi_) : feladat(std::move(feladat_)), vonat(std::move(vonat_)), kocsi(std::move(kocsi_)) {}

Akcio::Akcio()=default;

void Akcio::kiir()
{
    if (feladat=="felpakol" ||feladat=="lepakol")
    {
        cout << feladat << " ";
        cout << kocsi.get_nev() << " " << cucc.first.get_nev() << " "<< cucc.second << endl;
    }
    if (feladat=="lecsatol" ||feladat=="felcsatol")
    {
        cout << feladat << " ";
        cout << kocsi.get_nev() << " " << vonat.get_nev() << endl;
    }
}

string Akcio::get_feladat() const
{
    return feladat;
}

Kocsi Akcio::get_kocsi() const
{
    return kocsi;
}

Vonat Akcio::get_vonat() const
{
    return vonat;
}

pair<Cucc, int> Akcio::get_cucc() const
{
    return cucc;
}
