#include "Kocsi.h"

#include <utility>

Kocsi::Kocsi() = default;

Kocsi::Kocsi(string nev_, int kapacitas_, string allomas_) : nev(std::move(nev_)), kapacitas(kapacitas_), allomas(std::move(allomas_)) {}

bool Kocsi::operator<(const Kocsi& kocsi) const
{
    return nev < kocsi.nev;
}

void Kocsi::kiir(Kocsi& kocsi)
{
    cout << nev << " " << kapacitas << " " << allomas << endl;
}

string Kocsi::get_nev() const
{
    return nev;
}

int Kocsi::get_kapacitas() const
{
    return kapacitas;
}

string Kocsi::get_allomas() const
{
    return allomas;
}

bool Kocsi::operator==(Kocsi rhs)
{
    return (nev == rhs.get_nev());
}